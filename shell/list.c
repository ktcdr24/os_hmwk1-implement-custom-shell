/*
#include <stdlib.h>
*/
#define LIST_MAX_LENGTH 100

struct node {
	char *data;
	struct node *next;
};

void list_add(struct node **head, char *data)
{
	struct node *temp;
	temp = *head;
	if (temp == NULL) {
		temp = (struct node *)malloc(sizeof(struct node));
		temp->data = data;
		temp->next = NULL;
		*head = temp;
	} else {
		while (temp->next != NULL)
			temp = temp->next;
		struct node *newNode;
		newNode = (struct node *)malloc(sizeof(struct node));
		newNode->data = data;
		newNode->next = NULL;
		temp->next = newNode;
	}
}

int list_remove_first(struct node **head)
{
	struct node *temp;
	if (temp == NULL)
		return -1;
	else {
		temp = *head;
		*head = temp->next;
		free(temp);
		return 1;
	}
}

int list_remove(struct node **head, char *data)
{
	struct node *temp;
	temp = *head;
	if (temp == NULL)
		return -1;
	else {
		struct node *prev;
		while (temp != NULL) {
			if (strcasecmp(temp->data, data) == 0) {
				if (temp == *head)
					*head = temp->next;
				else
					prev->next = temp->next;
				free(temp);
				return 1;
			} else {
				prev = temp;
				temp = temp->next;
			}
		}
		return 0;
	}
}

int list_length(struct node *head)
{
	int length = 0;
	while (head != NULL) {
		length++;
		head = head->next;
	}
	return length;
}

void list_add_cap(struct node **head, char *data)
{
	int listLength = list_length(*head);
	if (listLength == LIST_MAX_LENGTH) {
		list_remove_first(head);
		list_add(head, data);
	} else {
		list_add(head, data);
	}
}

char *list_retrieve(struct node *head, int loc)
{
	if (head == NULL || loc < 1)
		return NULL;
	int i;
	for (i = 1; i < loc; i++) {
		if (head == NULL)
			return NULL;
		head = head->next;
	}
	if (head == NULL)
		return NULL;
	else
		return head->data;
}

void list_print(struct node *head, char *delim)
{
	if (head == NULL)
		printf("List:: isEmpty\n");
	else {
		printf("List:: ");
		while (head != NULL) {
			printf("%s%s", head->data, delim);
			head = head->next;
		}
		printf("\n");
	}
}

/*
int main() {
	struct node *l;
	l = NULL;
	list_add_cap(&l, "ab");
	list_add_cap(&l, "cd");
	list_add_cap(&l, "e");
	list_add_cap(&l, "fghijk");
	list_add_cap(&l, "l");
	list_add_cap(&l, "mn");
	printf("length=%d\n", list_length(l));
	list_print(l, ",");

	int i;
	for(i = 0; i < 10; i++)
		printf("[%d] == %s\n", i, list_retrieve(l, i));

	//list_remove(&l, "ab");
	list_remove(&l, "cd");
	list_add_cap(&l, "op");
	//list_remove(&l, "l");
	//printf("length=%d\n", list_length(l));
	//list_print(l, ",");
	//list_remove_first(&l);
	//printf("length=%d\n", list_length(l));
	//list_print(l);

	//int i;
	for(i = 0; i < 10; i++)
		printf("[%d] == %s\n", i, list_retrieve(l, i));
}
*/
