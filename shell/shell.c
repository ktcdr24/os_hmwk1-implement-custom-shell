#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>
#include "list.c"

char *inputDelim = " ";
struct node *path;
struct node *history;

char *getInput()
{
	char *input;
	input = malloc(512 * sizeof(char));
	printf("w4118_sh:$ ");
	if (fgets(input, 512, stdin) == NULL) {
		printf("error: error happened in fgets");
		return NULL;
	}
	/* if nothing is entered by the user */
	if (strlen(input) == 1)
		return NULL;
	if (input[strlen(input)-1] == '\n')
		input[strlen(input)-1] = '\0';
	return input;
}

int countTokens(char *input)
{
	char *temp = (char *)malloc(strlen(input) + 1 * sizeof(char));
	strcpy(temp, input);
	char *tempToken;
	int numTokens = 0;
	tempToken = strtok(temp, inputDelim);
	while (tempToken != NULL) {
		numTokens++;
		tempToken = strtok(NULL, inputDelim);
	}
	return numTokens;
}

char **parseInput(char *input, int numTokens)
{
	char **inputTokens;
	/* for inputTokens, we allocate 1 more than the number of tokens
		because, we need to keep the last token as NULL.
		this is needed to traverse the (char **) string martix
		without the knowledge of the number of number of
		string present in (char **)
		i.e. traverse a martix without knowing number of
		rows in the matrix
	inputTokens = (char **)malloc(numTokens + 1 * sizeof(char *));
	*/
	inputTokens = (char **)malloc(numTokens * sizeof(char *));
	char *temp = (char *)malloc((strlen(input) + 1) * sizeof(char));
	strcpy(temp, input);
	char *tempToken;
	int i = 0;
	tempToken = strtok(temp, inputDelim);
	while (tempToken != NULL) {
		inputTokens[i] = (char *)malloc((strlen(tempToken) + 1) \
				* sizeof(char));
		strcpy(inputTokens[i], tempToken);
		tempToken = strtok(NULL, inputDelim);
		i++;
	}
	inputTokens[i] = NULL;
	return inputTokens;
}

void executeCd(char **inputTokens, int numTokens)
{
	char *arg;
	if (numTokens == 1)
		arg = NULL;
	else if (numTokens == 2)
		arg = inputTokens[1];

	if (chdir(arg) == -1)
		printf("error: %s\n", strerror(errno));
}

void printPath(char *delim)
{
	struct node *temp = path;
	while (temp != NULL) {
		printf("%s", temp->data);
		temp = temp->next;
		if (temp != NULL)
			printf("%s", delim);
	}
	printf("\n");
}

void executePathCmd(char **inputTokens, int numTokens)
{
	if (numTokens == 1) {
		printPath(":");
	} else if (numTokens > 1 && (strcmp(inputTokens[1], "+") == 0)) {
		int i;
		for (i = 2; i < numTokens; i++)
			list_add(&path, inputTokens[i]);
	} else if (numTokens > 1 && (strcmp(inputTokens[1], "-") == 0)) {
		int i;
		for (i = 2; i < numTokens; i++)
			list_remove(&path, inputTokens[i]);
	}
}

char *appendPathToCmd(char *path, char *cmd)
{
	char *pathCmd;
	int cmdLen = strlen(cmd);
	if (path == NULL) {
		pathCmd = (char *)malloc((cmdLen + 1) * sizeof(char));
		strcat(pathCmd, cmd);
		return pathCmd;
	}
	int pathLen = strlen(path);
	if (path[pathLen - 1] == '/') {
		pathCmd = (char *)malloc((pathLen + cmdLen + 1) \
				* sizeof(char));
		strcat(pathCmd, path);
		strcat(pathCmd, cmd);
		return pathCmd;
	} else {
		pathCmd = (char *)malloc((pathLen + cmdLen + 2) \
				* sizeof(char));
		strcat(pathCmd, path);
		strcat(pathCmd, "/");
		strcat(pathCmd, cmd);
		return pathCmd;
	}
}

char **prepareArgs(char *pathCmd, char **inputTokens, int numTokens)
{
	char **args;
	args = (char **)malloc((numTokens + 1) * sizeof(char *));
	if (pathCmd == NULL) {
		args[0] = (char *)malloc((strlen(inputTokens[0]) + 1) \
				*  sizeof(char));
		strcpy(args[0], inputTokens[0]);
	} else {
		args[0] = (char *)malloc(strlen(pathCmd) + 1);
		strcpy(args[0], pathCmd);
	}
	int i;
	for (i = 1; i < numTokens; i++) {
		args[i] = (char *)malloc((strlen(inputTokens[i]) + 1) \
				* sizeof(char));
		strcpy(args[i], inputTokens[i]);
	}
	args[i] = (char *)malloc(sizeof(char *));
	args[i] = (char *)0;
	return args;
}

void doExecv(char *pathCmd, char **args)
{
	if (access(pathCmd, X_OK) == -1)
		return;
	
	if (execv(pathCmd, args) == -1)
		printf("error: %s\n", strerror(errno));
}


void executeCmd(char **inputTokens, int numTokens)
{
	struct node *temp;
	char *pathCmd, *cmd, **args;

	cmd = inputTokens[0];
	temp = path;

	short pid;
	pid = fork();
	if (pid == -1)
		printf("error: %s\n", strerror(errno));
	else if (pid == 0) {
		//if (temp == NULL)
		//	printf("error: PATH is empty\n");

		pathCmd = appendPathToCmd(NULL, cmd);
		args = prepareArgs(NULL, inputTokens, numTokens);

		doExecv(pathCmd, args);

		while (temp != NULL) {
			pathCmd = appendPathToCmd(temp->data, cmd);
			args = prepareArgs(pathCmd, \
					inputTokens, numTokens);
			doExecv(pathCmd, args);
			temp = temp->next;
		}
		printf("error: command not found\n");
		exit(0);
	} else {
		while (pid != wait(0))
			;
	}
	return;
}

void printHistory()
{
	struct node *temp;
	temp = history;
	if (temp == NULL) {
		printf("error: history is empty\n");
		return;
	}
	int i = 1;
	while (temp != NULL) {
		printf("[%d] %s\n", i, temp->data);
		temp = temp->next;
		i++;
	}
}

int executeInput(char **inputTokens, int numTokens)
{
	char *cmd = inputTokens[0];
	if (strcasecmp(cmd, "exit") == 0)
		return 1;
	else if (strcasecmp(cmd, "cd") == 0)
		executeCd(inputTokens, numTokens);
	else if (strcasecmp(cmd, "path") == 0)
		executePathCmd(inputTokens, numTokens);
	else if (strcasecmp(cmd, "history") == 0)
		printHistory();
	/*
	else if (cmd[0] == '!')
		executeHistoryCmd(inputTokens, numTokens);
	*/
	else
		executeCmd(inputTokens, numTokens);
	return 0;
}

void executeHistoryCmd(char **inputTokens, int numTokens)
{
	int nVal = 0;
	int i, j;
	int len = strlen(inputTokens[0]);
	for (i = 1; i < len; i++) {
		j = inputTokens[0][i] - '0';
		nVal = (nVal * 10) + j;
	}
	char *nCmd = list_retrieve(history, nVal);
	printf("%s\n", nCmd);
	int nCmdNumTokens = countTokens(nCmd);
	char **nCmdTokens = parseInput(nCmd, nCmdNumTokens);
	list_add_cap(&history, nCmd);
	executeInput(nCmdTokens, nCmdNumTokens);
}

int main(int argc, char **argv)
{
	char *input;

	while (1) {
		input = getInput();
		if (input == NULL)
			continue;
		int numTokens = countTokens(input);
		char **inputTokens = parseInput(input, numTokens);

		if (inputTokens[0][0] == '!') {
			executeHistoryCmd(inputTokens, numTokens);
			continue;
		}

		/* if the cmd is NOT "history", add it to history list */
		if (strcasecmp(inputTokens[0], "history") != 0)
			list_add_cap(&history, input);
		int exeResult = executeInput(inputTokens, numTokens);
		if (exeResult == 1)
			break;
		/*
		int i;
		for(i = 0; i < numTokens; i++) {
			printf("\t>tokens[%d] = %s\n", i, inputTokens[i]);
		}
		*/
	}

	return 0;
}

