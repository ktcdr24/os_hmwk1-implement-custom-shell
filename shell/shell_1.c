#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

char *inputDelim = " ";

void executeCmd(char *input);

int main(int argc, char **argv)
{
	char input[512];
	int inputLength;

	char *pwd = "<pwd>";

	printf("Welcome to my custom shell implementation!!!\n");

	while (1) {
		printf("w4118_sh:%s$ ", pwd);
		fgets(input, 512, stdin);

		inputLength = strlen(input);
		if (input[inputLength-1] == '\n')
			input[inputLength-1] = '\0';

		printf("\t -> the command entered is: %s\n", input);

		char *tempInput = malloc(strlen(input));
		strcpy(tempInput, input);
		char *tempCmd = strtok(tempInput, inputDelim);

		if (strcmp(tempCmd, "cd") == 0) {
			printf("\t now do cd\n");
		} else if (strcmp(tempCmd, "exit") == 0) {
			printf("\t custom shell is now exiting\n");
			break;
		} else {
			executeCmd(input);
		}
	}

	return 0;
}

void executeCmd(char *input) {
	short pid;
	pid = fork();
	if (pid == -1) {
		/* fork failed */
		printf("[ERROR] fork return with pid=-1, now aborting the exec of the command: %s\n", input);
		return;
	} else if (pid == 0) {
		/* child process */
		//printf("\t inside child process :: %s\n", input);

		int numTokens = 0;
		char *nextToken = NULL;
		char tempInput[strlen(input)];
		strcpy(tempInput, input);
		nextToken = strtok(tempInput, inputDelim);
		while (nextToken != NULL) {
			numTokens++;
			printf("\t\t > %s\n", nextToken);
			nextToken = strtok(NULL, inputDelim);
		}
		printf("\t > %d", numTokens);

		char *inputSplitAry[numTokens];
		int i = 0;
		strcpy(tempInput, input);
		nextToken = strtok(tempInput, inputDelim);
		while (nextToken != NULL) {
			printf("\t\t > %s\n", nextToken);
			inputSplitAry[i] = malloc(strlen(nextToken));
			inputSplitAry[i] = nextToken;
			i++;
			nextToken = strtok(NULL, inputDelim);
		}

		for (i = 0; i < numTokens; ++i)
			printf("\t\t -> %d = %s\n", i, inputSplitAry[i]);

		//char *cmd = inputSplitAry[0];
		char *cmdArgs[numTokens + 1];
		for(i = 0; i < numTokens; ++i) {
			cmdArgs[i] = malloc(strlen(inputSplitAry[i]));
			cmdArgs[i] = inputSplitAry[i];
		}
		cmdArgs[i+1] = (char *)0;

		execv("/bin/ls", cmdArgs);
		exit(0);
	} else {
		/* parent process */
		printf("\t inside parent process\n");
		while (pid != wait(0))
			;
	}
}
